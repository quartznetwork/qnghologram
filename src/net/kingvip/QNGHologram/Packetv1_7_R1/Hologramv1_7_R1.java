package net.kingvip.QNGHologram.Packetv1_7_R1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.kingvip.QNGHologram.Packet.Hologram;
import net.minecraft.server.v1_7_R1.EntityHorse;
import net.minecraft.server.v1_7_R1.EntityPlayer;
import net.minecraft.server.v1_7_R1.EntityWitherSkull;
import net.minecraft.server.v1_7_R1.PacketPlayOutAttachEntity;
import net.minecraft.server.v1_7_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_7_R1.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_7_R1.WorldServer;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_7_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_7_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Hologramv1_7_R1 implements Hologram{

	@Override
	public List<Integer> showHologram(List<String> lines, Player p,  Location loc) {
		List<Integer> ids = new ArrayList<Integer>();
		Location first = loc.clone().add(0, (lines.size() / 2) * 0.23, 0);
		for (int i = 0; i < lines.size(); i++) {
			ids.addAll(showLine(p, first.clone(), lines.get(i)));
			first.subtract(0, 0.23, 0);
		}
		return ids;
	}

	@Override
	public void destroyHologram(List<Integer> ids, Player p) {
		int[] ints = new int[ids.size()];
		for (int j = 0; j < ints.length; j++) {
			ints[j] = ids.get(j);
		}
		PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(ints);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
	}

	@Override
	public List<Integer> showLine(Player p, Location loc, String text) {
		WorldServer world = ((CraftWorld) loc.getWorld()).getHandle();
		EntityWitherSkull skull = new EntityWitherSkull(world);
		skull.setLocation(loc.getX(), loc.getY() + 1 + 55, loc.getZ(), 0, 0);
		((CraftWorld) loc.getWorld()).getHandle().addEntity(skull);
		EntityHorse horse = new EntityHorse(world);
		horse.setLocation(loc.getX(), loc.getY() + 55, loc.getZ(), 0, 0);
		horse.setAge(-1700000);
		horse.setCustomName(text);
		horse.setCustomNameVisible(true);
		PacketPlayOutSpawnEntityLiving packedt = new PacketPlayOutSpawnEntityLiving(horse);
		EntityPlayer nmsPlayer = ((CraftPlayer) p).getHandle();
		nmsPlayer.playerConnection.sendPacket(packedt);
		PacketPlayOutAttachEntity pa = new PacketPlayOutAttachEntity(0, horse, skull);
		nmsPlayer.playerConnection.sendPacket(pa);
		return Arrays.asList(skull.getId(), horse.getId());
	}
}