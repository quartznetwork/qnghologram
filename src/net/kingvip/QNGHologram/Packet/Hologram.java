package net.kingvip.QNGHologram.Packet;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface Hologram{

	public List<Integer> showHologram(List<String> lines, Player p, Location loc);

	public void destroyHologram(List<Integer> ids, Player p);

	public List<Integer> showLine(Player p, Location loc, String text);
}

