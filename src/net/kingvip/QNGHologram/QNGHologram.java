package net.kingvip.QNGHologram;

import java.util.logging.Logger;

import net.kingvip.QNGHologram.Packet.Hologram;
import net.kingvip.QNGHologram.Util.Reflection;
import net.kingvip.QNGHologram.Util.Enum.VersionType;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class QNGHologram extends JavaPlugin {

	public Logger log;
	public JavaPlugin plugin;

	private Reflection Reflection;
	private static Hologram Hologram;

	@Override
	public void onEnable() {

		/**
		 * Register
		 */
		Reflection = new Reflection(this);

		/**
		 * Version Check
		 */
		checkVersion();

		/**
		 * New Instance
		 */
		registerInstance();
	}

	@Override
	public void onDisable(){

	}

	/**
	 * Get Class
	 */
	public Reflection getReflection() {
		return Reflection;
	}
	public static Hologram getHologram() {
		return Hologram;
	}

	/**
	 * バージョンチェック
	 */
	private void checkVersion(){
		VersionType v = getReflection().getVersion();
		if(v.equals(VersionType.v1_7)){
			this.getLogger().info("VersionCheck: v1_7");
		}else{
			this.getLogger().info("VersionCheck: FILED");
			this.getLogger().info("使用しているCraftBukkitバージョンはサポートされていません!");
			Bukkit.getPluginManager().disablePlugins();
		}
	}

	/**
	 * インスタンスの作成
	 */
	private void registerInstance() {
		try{
			@SuppressWarnings("unchecked")
			Class<? extends Hologram> h = (Class<? extends Hologram>) Class.forName("net.kingvip.QNGHologram.Packet" + getReflection().getCraftBukkitVersion() + ".Hologram" + getReflection().getCraftBukkitVersion());
			Hologram = h.newInstance();
			this.getLogger().info("RegisterInstance: OK");
		}catch (Exception e){
			this.getLogger().info("RegisterInstance: FILED");
			this.getLogger().info("このバージョンに対応するクラスが見つかりません!!");
		}
	}
}